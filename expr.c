#include "def.h"
#include "parser.h"

Boolean type_equal(Schema schema1, Schema schema2)
{
  Pschema p1, p2;
  
  if(schema1.type != schema2.type)
    return(FALSE);
  if(schema1.type == TABLE)
  {
    for(p1 = schema1.next, p2 = schema2.next; p1 != NULL && p2 != NULL; p1= p1->next, p2 = p2->next)
      if(p1->type != p2->type || !compatible(p1->name, p2->name))
        return(FALSE);
    return(p1 == NULL && p2 == NULL);
  }
  else
    return(TRUE);
}

Boolean compatible(char *name1, char *name2)
{
  return(name1 == NULL || name2 == NULL || name1 == name2);
}

Pschema clone_schema(Pschema pschema)
{
  Pschema clone, psch;
  
  clone = psch = (Pschema) newmem(sizeof(Schema));
  *psch = *pschema;
  while(pschema->next)
  {
    psch->next = (Pschema) newmem(sizeof(Schema));
    *(psch->next) = *(pschema->next);
    psch = psch->next;
    pschema = pschema->next;
  }
  return (clone);
}

Pschema append_schemas(Pschema psch1, Pschema psch2)
{
  Pschema head = psch1;
  
  while(psch1->next)
    psch1 = psch1->next;
  psch1->next = psch2;
  return(head);
}

// ------------------------ 

Boolean repeated_names(Pname n)
{
    Pname p1, p2;
    for(p1 = n ; p1 != NULL ; p1 = p1->next)
    {
        for(p2 = p1->next ; p2 != NULL ; p2 = p2->next)
        {
            if(p1->name == p2->name) {
                return TRUE;
            }
        }
    }

    return FALSE;
}

Boolean duplicated(char* name, Pschema schema)
{
    Pschema temp = schema;
    for(temp = schema ; temp != NULL ; temp = temp->next)
    {
        if(temp->name == name) {
            return TRUE;
        }
    }

    return FALSE;
}

Boolean homonyms(Pschema ps1, Pschema ps2)
{
    Pschema temp1, temp2;

    for(temp1 = ps1 ; temp1 != NULL ; temp1 = temp1->next)
    {
        for(temp2 = ps2 ; temp2 != NULL ; temp2 = temp2->next)
        {
            if(temp1->name == temp2->name) {
                return TRUE;
            }
        }
    }

    return FALSE;
}

Pschema atomic_type(Pnode p)
{
    if(p->type != N_ATOMIC_TYPE) {
        noderror(p);
    }

    return schemanode(NULL, p->value.ival);
}

Code attr_code(Pnode p, Pschema s)
{
    Code c = endcode();

    while(p = p->brother)
    {
        Code temp = makecode2(T_ATTR, get_attribute_offset(s->next, valname(p)), get_size(name_in_schema(valname(p), s)));
        c = (c.head == NULL ? temp : appcode(c, temp));
    }

    return c;
}

Pschema schemanode(char* name, int type)
{
    Pschema s = (Pschema)newmem(sizeof(Schema));
    s->next = NULL;
    s->type = type;
    s->name = name;
    return s;
}

Pschema table_type(Pnode p)
{
    if(p->type != N_TABLE_TYPE) {
        noderror(p);
    }

    Pschema table_schema = schemanode(NULL, TABLE);

    Pname attr_list = NULL;

    Pnode n;

    for(n = p->child ; n != NULL ; n = n->brother)
    {
        if(n->child->type != N_ATOMIC_TYPE) {
            noderror(n->child);
        }

        if(n->child->brother->type != N_ID) {
            noderror(n->child->brother);
        }

        Pschema attr_schema = atomic_type(n->child);
        attr_schema->name = valname(n->child->brother);


        Pname elem = (Pname)newmem(sizeof(Name));
        elem->name = attr_schema->name;
        elem->next = NULL;

        if(attr_list == NULL) {
            attr_list = elem;
        }
        else
        {
            Pname temp = attr_list;
            while(temp->next) {
                temp = temp->next;
            }
            
            temp->next = elem;
        }

        table_schema = append_schemas(table_schema, attr_schema);
    }

    if(repeated_names(attr_list)) {
        semerror(p, "Duplicate attribute name");
    }

    Pname temp;
    while(attr_list)
    {
        temp = attr_list->next;
        freemem(attr_list, sizeof(Name));
        attr_list = temp;
    }

    return table_schema;
}

Code expr(Pnode root, Pschema pschema)
{
    pschema->name = NULL;
    pschema->next = NULL;
    
    switch(root->type)
    {
        case N_ID :             return expr_ID(root, pschema);
        case N_INTCONST:        return expr_INTCONST(root, pschema);
        case N_STRCONST:        return expr_STRCONST(root, pschema);
        case N_BOOLCONST:       return expr_BOOLCONST(root, pschema);
        case N_TABLE_CONST:     return expr_TABLE_CONST(root, pschema);
        case N_COMP_EXPR:       return expr_COMP_EXPR(root, pschema);
        case N_LOGIC_EXPR:      return expr_LOGIC_EXPR(root, pschema);
        case N_MATH_EXPR:       return expr_MATH_EXPR(root, pschema);
        case N_NEG_EXPR:        return expr_NEG_EXPR(root, pschema);
        case N_JOIN_EXPR:       return expr_JOIN_EXPR(root, pschema);
        case N_PROJECT_EXPR:    return expr_PROJECT_EXPR(root, pschema);
        case N_SELECT_EXPR:     return expr_SELECT_EXPR(root, pschema);
        case N_EXTEND_EXPR:     return expr_EXTEND_EXPR(root, pschema);
        case N_UPDATE_EXPR:     return expr_UPDATE_EXPR(root, pschema);
        case N_RENAME_EXPR:     return expr_RENAME_EXPR(root, pschema);

        default: noderror(root);
    }

    return endcode();
}

Code expr_ID(Pnode root, Pschema pschema)
{
    int offset, context;
    Pschema s = name_in_constack(valname(root), &offset, &context);

    if(s != NULL)
    {
        pschema->type = s->type;
        pschema->name = valname(root);
        return makecode3(T_LAT, offset, context, get_size(s));
    }

    Psymbol psymbol = lookup(valname(root));
    if(psymbol != NULL)
    {
        *pschema = psymbol->schema;
        return makecode1(T_LOB, psymbol->oid);
    }
    
    semerror(root, "Cannot find id");
}

Code expr_INTCONST(Pnode root, Pschema pschema)
{
    pschema->type = INTEGER;
    return make_ldint(root->value.ival);
}

Code expr_STRCONST(Pnode root, Pschema pschema)
{
    pschema->type = STRING;
    return make_ldstr(valname(root));
}

Code expr_BOOLCONST(Pnode root, Pschema pschema)
{
    pschema->type = BOOLEAN;
    return make_ldint(root->value.ival);
}

Code expr_TABLE_CONST(Pnode root, Pschema pschema)
{
    pschema->type = TABLE;
    pschema->next = NULL;

    Code c1, c2;

    if(root->child->type == N_ATOMIC_TYPE)
    {
        Pnode pnode = root->child;

        pschema->next = (Pschema)newmem(sizeof(Schema));
        pschema->next->type = pnode->value.ival;
        Pschema temp = pschema->next;

        do
        {
            Pschema ps = (Pschema)newmem(sizeof(Schema));
            ps->type = pnode->value.ival;

            temp->next = ps;
            temp = temp->next;

            pnode = pnode->brother;
        }
        while(pnode != NULL);
    }
    else if(root->child->type == N_TUPLE_CONST)
    {
        Pnode pnode = root->child;

        pschema->next = (Pschema)newmem(sizeof(Schema));
        pschema->next->type = pnode->value.ival;
        Pschema stemp = pschema->next;

        do
        {
            Pschema ps = (Pschema)newmem(sizeof(Schema));
            
            Schema s;
            expr(pnode, &s);
            ps->type = s.type;

            stemp->next = ps;
            stemp = stemp->next;

            pnode = pnode->brother;
        }
        while(pnode != NULL);

        c1 = endcode();
        Pnode temp;
        int cont = 0;

        for(temp = root->child ; temp != NULL ; temp = temp->brother)
        {
            c1 = (c1.head == NULL ? tuple_const(temp, pschema->next) : appcode(c1, tuple_const(temp, pschema->next)));
            cont++;
        }

        c2 = makecode2(T_LDTAB, get_size(pschema), cont);
        if(cont > 0) {
            c2 = appcode(c2, c1);
        }
    }
    else {
        noderror(root->child);
    }

    return appcode(c2, makecode(T_ENDTAB));
}

Code expr_COMP_EXPR(Pnode root, Pschema pschema)
{
    Schema s1, s2;
    Code c1 = expr(root->child, &s1);
    Code c2 = expr(root->child->brother, &s2);
    
    pschema->type = BOOLEAN;

    if(type_equal(s1, s2) == NULL) {
        semerror(root, "Comparison expression needs two operands of the same type");
    }

    if(qualifier(root) == EQ || qualifier(root) == NE)
    {
        int op = (qualifier(root) == EQ ? T_EQU : T_NEQ);
        return concode(c1, c2, makecode(op), endcode());
    }
    
    if(qualifier(root) == '>' || qualifier(root) == '<' || qualifier(root) == GE || qualifier(root) == LE)
    {
        int op;

        switch(qualifier(root))
        {
            case '>':   op = (s1.type == INTEGER ? T_IGT : T_SGT); break;
            case GE :   op = (s1.type == INTEGER ? T_IGE : T_SGE); break;
            case '<':   op = (s1.type == INTEGER ? T_ILT : T_SLT); break;
            case LE :   op = (s1.type == INTEGER ? T_ILE : T_SLE); break;
        }

        return concode(c1, c2, makecode(op), endcode());
    }

    noderror(root);
}

Code expr_LOGIC_EXPR(Pnode root, Pschema pschema)
{
    Schema s1, s2;
    Code c1 = expr(root->child, &s1);
    Code c2 = expr(root->child->brother, &s2);

    if(s1.type != BOOLEAN || s2.type != BOOLEAN) {
        semerror(root, "Logic expression needs two boolean operands");
    }

    pschema->type = BOOLEAN;

    switch(qualifier(root))
    {
        case AND:   return concode(c1, makecode1(T_SKIPF, c2.size+2), c2, makecode1(T_SKIP, 2), makecode1(T_LDINT, 0), endcode());
        case OR:    return concode(c1, makecode1(T_SKIPF, 3), makecode1(T_LDINT, 1), makecode1(T_SKIP, c2.size+1), c2, endcode());
        default:    noderror(root);
    }
}

Code expr_MATH_EXPR(Pnode root, Pschema pschema)
{
    Schema s1, s2;

    Code c1 = expr(root->child, &s1);
    Code c2 = expr(root->child->brother, &s2);
    
    if(s1.type != INTEGER || s2.type != INTEGER) {
        semerror(root, "Math expression needs two integer operands");
    }
    
    pschema->type = INTEGER;

    int op;
    switch(qualifier(root))
    {
        case '+':   op = T_PLUS; break;
        case '-':   op = T_MINUS; break;
        case '*':   op = T_TIMES; break;
        case '/':   op = T_DIV; break;
        default:    noderror(root);
    }

    return concode(c1, c2, makecode(op), endcode());
}

Code expr_NEG_EXPR(Pnode root, Pschema pschema)
{
    Schema s1;
    Code c1 = expr(root->child, &s1);

    if(qualifier(root) == NOT)
    {
        if(s1.type != BOOLEAN) {
            semerror(root->child, "Negation expression needs a booleand operand");
        }

        pschema->type = BOOLEAN;
        return appcode(c1, makecode(T_NEG));
    }

    if(qualifier(root) == '-')
    {
        if(s1.type != INTEGER) {
            semerror(root->child, "Change of sign needs an integer operand");
        }
        
        pschema->type = INTEGER;
        return appcode(c1, makecode(T_UMI));
    }

    noderror(root);
}

Code expr_JOIN_EXPR(Pnode root, Pschema pschema)
{
    Schema s1, s2;
    Code c1 = expr(root->child, &s1);
    Code c2 = expr(root->child->brother->brother, &s2);

    if(s1.type != TABLE || s2.type != TABLE) {
        semerror(root, "Join expression needs table operands");
    }

    if(homonyms(s1.next, s2.next)) {
        semerror(root, "Join expression has common attribute names");
    }

    pschema->type = TABLE;
    pschema->next = append_schemas(clone_schema(s1.next), clone_schema(s2.next));

    push_context(pschema->next);
    Schema s3;
    Code c3 = expr(root->child->brother, &s3);
    pop_context();

    if(s3.type != BOOLEAN) {
        semerror(root, "Join expression needs a boolean predicate");
    }

    return concode(c1, c2, makecode1(T_JOIN, c3.size), c3, makecode1(T_ENDJOIN, c3.size), endcode());
}

Code expr_PROJECT_EXPR(Pnode root, Pschema pschema)
{
    Schema s1;
    Code c1 = expr(root->child, &s1);

    if(s1.type != TABLE) {
        semerror(root->child, "Project expression needs a table operand");
    }

    int list_length = 0;
    Pname names = id_list(root->child->brother, &list_length);

    if(repeated_names(names)) {
        semerror(root->child, "Repeated names found in the list");
    }

    pschema->type = TABLE;

    Pschema pos = NULL;
    Pname n;

    for(n = names ; n != NULL ; n = n->next)
    {
        Pschema temp = name_in_schema(n->name, s1.next);

        if(temp == NULL)
        {
            char *err;
            sprintf(err, "Unknown attribute %s in project expression", n->name);
            semerror(root->child, err);
        }

        if(pos == NULL)
        {
            pos = schemanode(temp->name, temp->type);
            pschema->next = pos;
        }
        else
        {
            pos->next = schemanode(temp->name, temp->type);
            pos = pos->next;
        }
    }
    
    return concode(c1, makecode1(T_PROJ, list_length), attr_code(root->child->brother, &s1), makecode(T_ENDPROJ), makecode(T_REMDUP), endcode());
}

Code expr_SELECT_EXPR(Pnode root, Pschema pschema)
{
    Schema s1, s2;
    Code c1 = expr(root->child->brother, &s1);
    push_context(s1.next);

    Code c2 = expr(root->child, &s2);
    pop_context();

    if(s1.type != TABLE) {
        semerror(root->child->brother, "Selection expression needs a table operand");
    }

    if(s2.type != BOOLEAN) {
        semerror(root->child, "Selection expression needs a boolean predicate");
    }

    switch(qualifier(root))
    {
        case SELECT:
            pschema->type = TABLE;
            pschema->next = clone_schema(s1.next);
            return concode(c1, makecode1(T_SEL, c2.size), c2, makecode1(T_ENDSEL, c2.size), endcode());
            
        case EXISTS:
            pschema->type = BOOLEAN;
            return concode(c1, makecode1(T_EXS, c2.size), c2, makecode1(T_ENDEXS, c2.size), endcode());
            
        case ALL:
            pschema->type = BOOLEAN;
            return concode(c1, makecode1(T_ALL, c2.size), c2, makecode1(T_ENDALL, c2.size), endcode());
            
        default: noderror(root);
    }
}

Code expr_EXTEND_EXPR(Pnode root, Pschema pschema)
{
    Schema s1;
    Code c1 = expr(root->child, &s1);
    push_context(s1.next);

    if(s1.type != TABLE) {
        semerror(root->child, "Extend expression needs a table operand");
    }

    Schema s2;
    Code c2 = expr(root->child->brother->brother->brother, &s2);
    pop_context();

    Pschema tschema = atomic_type(root->child->brother);
    if(type_equal(*tschema, s2) == FALSE) {
        semerror(root->child->brother, "Extend expression needs correspondent attributes and expressions with the same type");
    }

    char* name = valname(root->child->brother->brother);
    if(name_in_schema(name, s1.next)) {
        semerror(root->child->brother->brother, "Attribute name in extend must be new");
    }

    pschema->type = TABLE;

    Pschema attr = schemanode(name, tschema->type);
    pschema->next = append_schemas(clone_schema(s1.next), attr);

    return concode(c1, makecode2(T_EXT, get_size(attr), c2.size), c2, makecode1(T_ENDEXT, c2.size), endcode());
}

Code expr_UPDATE_EXPR(Pnode root, Pschema pschema)
{
    Schema s1, s2;
    Code c1 = expr(root->child, &s1);
    push_context(s1.next);
    Code c2 = expr(root->child->brother->brother, &s2);
    pop_context();

    if(s1.type != TABLE) {
        semerror(root, "Update expression needs a table operand");
    }

    Pschema s;
    if((s = name_in_schema(valname(root->child->brother), s1.next)) == NULL)
    {
        char *err;
        sprintf(err, "Cannot find attribute %s in update expression", s->name);
        semerror(root->child->brother, err);
    }

    if(!type_equal(s2, *s)) {
        semerror(root->child->brother->brother, "Attribute type and expression type must be equal in update");
    }

    pschema->type = TABLE;
    pschema->next = clone_schema(s1.next);

    return concode(c1, makecode3(T_UPD, get_attribute_offset(s1.next, s->name), get_size(s), c2.size), c2, makecode1(T_ENDUPD, c2.size), makecode(T_REMDUP), endcode());
}

Code expr_RENAME_EXPR(Pnode root, Pschema pschema)
{
    Schema s1;
    Code c1 = expr(root->child, &s1);

    if(s1.type != TABLE) {
        semerror(root->child, "Rename expressione needs a table operand");
    }

    int cont = 0;
    Pname names = id_list(root->child->brother, &cont);

    if(repeated_names(names)) {
        semerror(root->child, "Repeated names found in the list");
    }

    Pschema temp;
    for(temp = s1.next ; temp != NULL ; temp = temp->next) {
        cont--;
    }
    if(cont != 0) {
        semerror(root->child, "Different number of attributes in rename expression");
    }

    temp = pschema->next = clone_schema(s1.next);
    
    Pname n;
    for(n = names ; n != NULL ; n = n->next)
    {
        temp->name = n->name;
        temp = temp->next;
    }

    return c1;
}
