#include "def.h"
#include "parser.h"

void semerror(Pnode p, char *message)
{
  printf("Line %d: %s\n", p->line, message);
  exit(-1);
}

int qualifier(Pnode p)
{
  return (p->value.ival);
}

char *valname(Pnode p)
{
  return (p->value.sval);
}

Code program(Pnode root)
{
  Code body = stat_list(root->child);
  
  return concode(makecode1(T_TCODE, body.size + 2),
		 body,
		 makecode(T_HALT),
		 endcode());
}

//---------------------------

Code stat_list(Pnode p)
{
    push_environment();

    Code c = stat(p->child);
    
    for(p = p->child->brother ; p != NULL ; p = p->brother) {
        c = appcode(c, stat(p));
    }

    int num_vars = 0;
    if ((num_vars = numobj_in_current_env()) > 0) {
        c = appcode(c, makecode1(T_POP, num_vars));
    }

    pop_environment();

    return c;
}

Code stat(Pnode p)
{
    switch(p->type)
    {
        case N_DEF_STAT:    return def_stat(p);
        case N_ASSIGN_STAT: return assign_stat(p);
        case N_IF_STAT:     return if_stat(p);
        case N_WHILE_STAT:  return while_stat(p);
        case N_READ_STAT:   return read_stat(p);
        case N_WRITE_STAT:  return write_stat(p);
        default:            noderror(p);
    }

    return endcode();
}

Code def_stat(Pnode p)
{
    Pname list;
    int id_list_num = 0;
    list = id_list(p->child->brother, &id_list_num);

    Pname n;
    for(n = list ; n != NULL ; n = n->next)
    {
        if(name_in_environment(n->name))
        {
            char *err;
            sprintf(err, "Cannot redefine variable %s", n->name);
            semerror(p, err);
        }
        
        insert_name_into_environment(n->name);
    }

    int op; // operator
    switch(p->child->type)
    {
        case N_ATOMIC_TYPE: op = T_NEWATOM; break;
        case N_TABLE_TYPE:  op = T_NEWTAB;  break;
        default:            noderror(p->child);
    }

    Code c;
    Schema s;

    c.head = NULL;

    for(n = list; n != NULL; n = n->next)
    {
        s = type(p->child);
        s.name = n->name;
        insert(s);

        if(c.head != NULL) {
            c = appcode(c, makecode1(op, get_size(&s)));
        } else {
            c = makecode1(op, get_size(&s));
        }
    }

    Pname temp;
    while(list)
    {
        temp = list->next;
        freemem(list, sizeof(Name));
        list = temp;
    }

    return c;
}

Code assign_stat(Pnode p)
{
    Psymbol psymbol = lookup(valname(p->child));

    if(psymbol == NULL)
    {
        char* err;
        sprintf(err, "Undefined varible %s", valname(p->child));
        semerror(p->child, err);
    }

    Pschema pschema = clone_schema(&psymbol->schema);

    Code c = expr(p->child->brother, pschema);

    if(type_equal(psymbol->schema, *pschema) == FALSE)
    {
        char* err;
        sprintf(err, "Incompatible types in assignment for variable %s", valname(p->child));
        semerror(p->child->brother, err);
    }

    Pschema temp;
    while(pschema)
    {
        temp = pschema->next;
        freemem(pschema, sizeof(Schema));
        pschema = temp;
    }

    return concode(c, makecode1(T_STO, psymbol->oid), endcode());
}

Code if_stat(Pnode p)
{
    Schema schema;

    Code condition_code = expr(p->child, &schema);
    Code then_code = stat_list(p->child->brother);

    if(schema.type != BOOLEAN) {
        semerror(p->child, "If condition has to be a boolean");
    }

    if(p->child->brother->brother == NULL) { // if then ...
        return concode(condition_code, makecode1(T_SKIPF, then_code.size+1), then_code, endcode());
    }
    else // if then ... else ...
    {
        Code else_code = stat_list(p->child->brother->brother);

        return concode(condition_code, makecode1(T_SKIPF, then_code.size+2), then_code, makecode1(T_SKIP, else_code.size+1), else_code, endcode());
    }
}

Code while_stat(Pnode p)
{
    Schema schema;
    Code condition_code = expr(p->child, &schema);
    Code body_code = stat_list(p->child->brother);

    if(schema.type != BOOLEAN) {
        semerror(p->child, "While condition has to be a boolean");
    }

    return concode(condition_code, makecode1(T_SKIPF, body_code.size+2), body_code, makecode1(T_SKIP, -(condition_code.size+body_code.size+1)), endcode());
}

Code read_stat(Pnode p)
{
    Psymbol psymbol = lookup(valname(p->child->brother));
    if(psymbol == NULL) {
        semerror(p->child, "Unknown identifier");
    }

    int op = (p->child->child == NULL ? T_GET : T_FGET); // if not null with specifier

    if(op == T_GET) {
        return makecode2(op, psymbol->oid, get_format(psymbol->schema));
    } else {
        return appcode(specifier(p->child), makecode2(op, psymbol->oid, get_format(psymbol->schema)));
    }
}

Code specifier(Pnode p)
{
    Schema schema;

    Code c = expr(p->child, &schema);
    
    if(schema.type != STRING) {
        semerror(p->child, "Specifier has to be a string");
    }

    return c;
}

Code write_stat(Pnode p)
{
    Schema schema;
    Code c = expr(p->child->brother, &schema);

    int op = T_PRINT;

    if(p->child->child != NULL) // with specifier
    {
        c = appcode(c, specifier(p->child));
        op = T_FPRINT;
    }

    Pschema pschema = schema.next;
    Pschema temp;
    while(pschema)
    {
        temp = pschema->next;
        freemem(pschema, sizeof(Schema));
        pschema = temp;
    }

    return concode(c, makecode1(op, get_format(schema)), endcode());
}

Code tuple_const(Pnode p, Pschema s)
{
    Code c = endcode();

    Pnode n;
    for(n = p->child ; n != NULL ; n = n->brother)
    {
        int op;
        
        switch(n->type)
        {
            case N_INTCONST:
            case N_BOOLCONST:   op = T_IATTR; break;
            case N_STRCONST:    op = T_SATTR; break;
            default:            noderror(n);
        }

        c = (c.head == NULL ?  makecode1(op, n->value.ival) : appcode(c, makecode1(op, n->value.ival)));
    }

    Pnode pnode = p;
    s->next = (Pschema)newmem(sizeof(Schema));
    s->next->type = pnode->value.ival;
    Pschema stemp = s->next;

    do
    {
        Pschema ps = (Pschema)newmem(sizeof(Schema));
            
        Schema sc;
        expr(pnode, &sc);
        ps->type = sc.type;

        stemp->next = ps;
        stemp = stemp->next;

        pnode = pnode->brother;
    }
    while(pnode != NULL);

    if(type_equal(*s->next, *s) == FALSE) {
        semerror(p, "Incompatible tuples types in tuple constant");
    }
    
    return c;
}

/////////////////////////////////////

Pname id_list(Pnode p, int* num)
{
    if(p == NULL) {
        return NULL;
    }

    (*num)++;

    Pname list = (Pname)newmem(sizeof(Name));
    list->name = p->value.sval;
    list->next = id_list(p->brother, num);

    return list;
}

Schema type(Pnode p)
{
    Pschema ps;

    switch(p->type)
    {
        case N_ATOMIC_TYPE: ps = atomic_type(p);    break;
        case N_TABLE_TYPE:  ps = table_type(p);     break;
        default:            noderror(p);
    }

    freemem(ps, sizeof(Schema));
    return *ps;
}

